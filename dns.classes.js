(function (exports) {
'use strict';

var classes = exports.DNS_CLASSES = {
  IN:   0x01  //   1
};
// and in reverse
Object.keys(classes).forEach(function (key) {
  classes[classes[key]] = key;
});

}('undefined' !== typeof window ? window : exports));
