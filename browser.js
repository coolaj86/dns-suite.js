;(function () {
'use strict';

var dnsjs = window.DNSPacket;
// I got the test data by running this in the node console:
// JSON.stringify(fs.readFileSync('./test/fixtures/www.google.com.a.bin', null))
var arr = [
  191,164,129,128,0,1,0,5,0,0,0,0,3,119,119,119,6,103,111,111,103,108,101,3,99
 ,111,109,0,0,1,0,1,192,12,0,1,0,1,0,0,0,29,0,4,74,125,239,112,192,12,0,1,0,1,0
 ,0,0,29,0,4,74,125,239,116,192,12,0,1,0,1,0,0,0,29,0,4,74,125,239,113,192,12,0
 ,1,0,1,0,0,0,29,0,4,74,125,239,115,192,12,0,1,0,1,0,0,0,29,0,4,74,125,239,114
];
var ab = Uint8Array.from(arr).buffer;
var result = dnsjs.parse(ab);
console.log(result);

}());
