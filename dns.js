;(function (exports) {
'use strict';

var Parser = (exports.DNS_PARSER || require('./dns.parser.js').DNS_PARSER);
var Packer = (exports.DNS_PACKER || require('./dns.packer.js').DNS_PACKER);
//var classes = exports.DNS_CLASSES || require('./dns.classes.js').DNS_CLASSES;
//var types = exports.DNS_TYPES || require('./dns.types.js').DNS_TYPES;
var logged = {};

exports.DNSPacket = {
  parse: function (nb) {
    // backwards compat with node buffer
    var ab = nb;
    if (nb.buffer) {
      // TODO pass byteOffsets to unpacker
      ab = nb.buffer.slice(nb.byteOffset, nb.byteOffset + nb.byteLength);
    }
    var packet = Parser.unpack(ab);

    function tryParseRdata(record) {

      try {

        record = Parser.unpackRdata(ab, packet, record);
      } catch (e) {
        record.error = e;
        if (!/^support for dns/i.test(e.message)) {
          console.error('[Error] unpackRdata: ' + e.message);
        }
        else if (!logged[e.message]) {
          console.error('[Error] unpackRdata: ' + e.message);
          logged[e.message] = true;
        }
      }
    }

    packet.answer.forEach(tryParseRdata);
    packet.authority.forEach(tryParseRdata);
    packet.additional.forEach(tryParseRdata);

    return packet;
  }
  // Backwards compat
, write: function (json) {
    return Buffer.from(Packer.pack(json));
  }
, pack: function (json) {
    return Packer.pack(json);
  }
};

if ('undefined' !== typeof module) {
  // backwards compat node dns.js
  exports.Parser = exports.DNS_PARSER;
  exports.Packer = exports.DNS_PACKER;
}

}('undefined' !== typeof window ? window : exports));
