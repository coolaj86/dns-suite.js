'use strict';

module.exports.respond = function (socket, packets, rinfo) {
  var dns = require('../');
  var os = require('os');
  var queryname = '_cloud._tcp.local';

  console.log(packets);

  packets.forEach(function (packet) {
    // Only respond to queries, otherwise we'll end up responding to ourselves forever.
    if (packet.header.qr !== 0) {
      return;
    }

    packet.question.forEach(function (q) {
      if (queryname !== q.name) {
        return;
      }

      console.log('question', q.name, q.typeName, q.className, q.flag, q);
      var rpacket = {
        header: {
          id: packet.header.id
        , qr: 1
        , opcode: 0
        , aa: 1
        , tc: 0
        , rd: 0
        , ra: 0
        , res1:  0
        , res2:  0
        , res3:  0
        , rcode: 0
      , }
      , question: [q]
      , answer: []
      , authority: []
      , additional: []
      , edns_options: []
      };

      var myRndId = 'be1af7a';

      rpacket.answer.push({
        name: q.name
      , typeName: 'PTR'
      , ttl: 10
      , className: 'IN'
      , data: myRndId + '.' + queryname
      });

      var ifaces = os.networkInterfaces();
      //var llRe = /^(fe80|169)/i; // link-local
      Object.keys(ifaces).forEach(function (iname) {
        var iface = ifaces[iname];

        iface = iface.filter(function (pface) {
          // nix loopback, internal and ipv6 link-local (non-routable) and ipv4 link-local
          return !pface.internal;// && !(pface.scopeid > 1) && !llRe.test(pface.address);
        });

        iface.forEach(function (pface) {
          rpacket.additional.push({
            name: myRndId + '.' + q.name
          , typeName: ('IPv4' === pface.family ? 'A' : 'AAAA')
          , ttl: 10
          , className: 'IN'
          , address: pface.address // '_workstation._tcp.local'
          });
        });
      });

      rpacket.additional.push({
        name: myRndId + '.' + queryname
      , typeName: 'SRV'
      , ttl: 10
      , className: 'IN'
      , priority: 1
      , weight: 0
      , port: 443
      , target: myRndId + ".local"
      });
      rpacket.additional.push({
        name: myRndId + '.' + '_device-info._tcp.local'
      , typeName: 'TXT'
      , ttl: 10
      , className: 'IN'
      , data: ["model=CloudHome1,1", "dappsvers=1"]
      });

      console.log('');
      console.log('START JSON PACKET');
      console.log(rpacket);
      var buf = dns.DNSPacket.write(rpacket);
      console.log(buf.toString('hex'));
      console.log('END JSON PACKET');
      console.log('');

      console.log('');
      console.log('START DNS PACKET');
      var pkt = dns.DNSPacket.parse(buf);
      console.log(pkt);
      console.log('END DNS PACKET');
      console.log('');
      socket.send(buf, rinfo.port, rinfo.address);
    });

    packet.answer.forEach(function (a) {
      console.log('answer', a.name, a.typeName, a.className, a.flag, a);
    });
    packet.authority.forEach(function (a) {
      console.log('authority', a.name, a.typeName, a.className, a.flag, a);
    });
    packet.additional.forEach(function (a) {
      console.log('additional', a.name, a.typeName, a.className, a.flag, a);
    });
  });
  console.log('\n');
};
