'use strict';

var dgram = require('dgram');

// SO_REUSEADDR and SO_REUSEPORT are set because
// the system mDNS Responder may already be listening on this port

var socket = dgram.createSocket({
  type: 'udp4'
, reuseAddr: true
});

var broadcast = '224.0.0.251'; // mdns
var port = 5353;               // mdns

socket.bind(port, function () {
  console.log('bound on', port);

  // mDNS must listen on the broadcast membership group address
  socket.setBroadcast(true);
  socket.addMembership(broadcast);

  // ... more stuff
});
