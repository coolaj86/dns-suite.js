'use strict';

var DNSPacket = require('../').DNSPacket;
var rnd = Math.floor(Math.random() * 65536);

var query = {
	header: {
		id: rnd,
		qr: 0,
		opcode: 0,
		aa: 0,
		rd: 1,
		ra: 0,
		rcode: 0
	},
	question: [
		{
			name: 'google.com',
			typeName: 'A',
			className: 'IN'
		}
	]
};

var buffer = DNSPacket.pack(query);

console.info(Buffer.from(buffer));
