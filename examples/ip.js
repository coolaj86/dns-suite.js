'use strict';

var os = require('os');

      var ifaces = os.networkInterfaces();
      var llRe = /^(fe80|169)/i; // link-local

      Object.keys(ifaces).forEach(function (iname) {
        var iface = ifaces[iname];

        iface = iface.filter(function (pface) {
          // nix loopback, internal and ipv6 link-local (non-routable) and ipv4 link-local
          return !pface.internal && !(pface.scopeid > 1) && !llRe.test(pface.address);
        });

        iface.forEach(function (pface) {
          console.log(pface);
        });
      });
