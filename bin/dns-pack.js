#!/usr/bin/env node
'use strict';

// EXAMPLE:
// node bin/dns-parse.js samples/a-0.mdns.bin

var path = require('path');
// pass a terminal arg
var filename = process.argv[2];
var outname = process.argv[3];
if (!filename) {
	console.error(
		'Usage: node bin/dns-pack.js <path/to/sample.json> <path/to/output.bin>'
	);
	console.error(
		'Example: node bin/dns-pack.js ./samples/services-0.mdns.json ./services-0.mdns.bin'
	);
	process.exit(1);
	return;
}
if (!outname) {
	console.warn('');
	console.warn(
		"Usage: node bin/dns-pack.js '" +
			filename +
			"' './" +
			path.basename(filename).replace(path.extname(filename), '') +
			".bin'"
	);
	console.warn('');
}

var PromiseA = require('bluebird');
var fs = PromiseA.promisifyAll(require('fs'));
var dnsjs = require('../').DNSPacket;

fs.readFileAsync(filename, null).then(function(nb) {
	//
	// current reference impl
	//
	//console.log(require('native-dns-packet').parse(nb));

	//
	// other reference impl
	//
	//console.log(require('dns-js').DNSPacket.parse(nb));

	// nb is a Uint8Array (ArrayBufferView) for nb.buffer
	// nb.buffer is the actual ArrayBuffer

	//var ab = nb.buffer.slice(nb.byteOffset, nb.byteOffset + nb.byteLength);
	var packet = dnsjs.write(JSON.parse(nb.toString('ascii')));

	//console.log('[packet]', nb.byteLength, 'bytes:');
	//console.log(JSON.stringify(packet, null, 2));

	//TODO hexdump packet
	var hexdump = require('@root/hexdump');
	var str = hexdump(packet);

	console.log(str);

	if (outname) {
		fs.writeFileSync(outname, packet, null);
		console.log('');
		console.log("wrote '" + outname + "'");
	}
});
