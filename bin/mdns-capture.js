#!/usr/bin/env node
'use strict';

// pass a terminal arg
var type = process.argv[2];
var count = parseInt(process.argv[3]) || 0;
if (!type) {
  console.error("Usage: mdns-capture.js <filename-prefix> [start-number]");
  console.error("Example: mdns-capture.js _service 0");
  console.error("Output: _service-0.mdns.bin");
  process.exit(1);
}

var PromiseA = require('bluebird');
var fs = PromiseA.promisifyAll(require('fs'));
var dgram = require('dgram');
var server = dgram.createSocket({
  type: 'udp4'
, reuseAddr: true
});


var handlers = {};
handlers.onError = function (err) {
    console.error("error:", err.stack);
    server.close();
};
handlers.onMessage = function (buffer) {
  var path = require('path');
  var filename = type + '-' + count + '.mdns.bin';
  var fullpath = path.join('samples', filename);

  count += 1;

  fs.writeFileAsync(fullpath, buffer).then(function () {
    console.log('wrote ' + buffer.length + ' bytes to ' + fullpath);
  });
};
handlers.onListening = function () {
  /*jshint validthis:true*/
  var server = this;
  console.log(server.address());

  server.setBroadcast(true);
  server.addMembership('224.0.0.251');

  console.log('CTRL+C to quit');
};


server.on('error', handlers.onError);
server.on('message', handlers.onMessage);
server.on('listening', handlers.onListening);

// 53 dns
// 5353 mdns
server.bind(5353);
