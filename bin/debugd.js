#!/usr/bin/env
(function () {
'use strict';

var cli = { port: 65153, address: null, udp6: false, bin: process.argv[2] };
var dgram = require('dgram');
var server = dgram.createSocket({
  type: cli.udp6 ? 'udp6' : 'udp4'
, reuseAddr: true
});
var handlers = {};
var bin = require('fs').readFileSync(cli.bin, null);

handlers.onMessage = function (nb, rinfo) {
  console.log('[DEBUG] got a message');

  // replace the id to match
  bin[0] = nb[0];
  bin[1] = nb[1];
  server.send(bin, rinfo.port, rinfo.address, function () {
    console.log('[DEBUG] sent response');
  });
};

handlers.onListening = function () {
  /*jshint validthis:true*/
  var server = this;

  if (cli.mdns || '224.0.0.251' === cli.nameserver) {
    server.setBroadcast(true);
    server.addMembership(cli.nameserver);
  }

  console.log('');
  console.log('Bound and Listening:');
  console.log(server.address().address + '#' + server.address().port + ' (' + server.type + ')');
};

server.bind({
  port: cli.port
, address: cli.address
});
server.on('listening', handlers.onListening);
server.on('message', handlers.onMessage);

}());
