#!/usr/bin/env node
'use strict';

// EXAMPLE:
// node bin/dns-parse.js samples/a-0.mdns.bin

// pass a terminal arg
var filename = process.argv[2];
if (!filename) {
  console.error("Usage: node bin/dns-parse.js <path/to/sample.bin>");
  console.error("Example: node bin/dns-parse.js ./samples/services-0.mdns.bin");
  process.exit(1);
}


var PromiseA = require('bluebird');
var fs = PromiseA.promisifyAll(require('fs'));
var dnsjs = require('../').DNSPacket;

fs.readFileAsync(filename, null).then(function (nb) {
  //
  // current reference impl
  //
  //console.log(require('native-dns-packet').parse(nb));


  //
  // other reference impl
  //
  //console.log(require('dns-js').DNSPacket.parse(nb));

  // nb is a Uint8Array (ArrayBufferView) for nb.buffer
  // nb.buffer is the actual ArrayBuffer

  var ab = nb.buffer.slice(nb.byteOffset, nb.byteOffset + nb.byteLength);
  var packet = dnsjs.parse(ab);

  console.log('[packet]', nb.byteLength, 'bytes:');
  console.log(JSON.stringify(packet, null, 2));
});
