mDNS Documentation

What is mDNS?

mDNS stands for Multicast Domain Name System. It's function is to resolve host names to IP addresses within small networkds that do not include a local name server. It is a zero-configuration service, using essentially the same programming interfaces, packet formats and operating semantics as the unicast Domain Name System (DNS). Although mDNS is designed to be stand-alone capable, it can work in concert with unicast DNS servers.

Full documentation of the protocol is located at: https://tools.ietf.org/html/rfc6762

Other helpful topics and documents:

- Zero Configuration Networking: https://tools.ietf.org/html/rfc6762#ref-Zeroconf
- Automatic link-local addressing: https://tools.ietf.org/html/rfc3927

				   https://tools.ietf.org/html/rfc4862
				   

DNS-format messages contain a header, a Question Section, then Answer, Authority, and additional Record Sections. The Answer, Authority, and Additional Record Sections all hold resource records.


mDNS is a datagram protocal that uses broadcast mode and membership groups.



What does mDNS in the Daplie System?

We are sending messages back and forth on a network that searches for any other mDNS compatible devices and asks that device if it is connected to a router or WiFi with a Daplie-compatible Cloud device.

If it is a Daplie-compatible Cloud device, then it will reply to the sender with some sort of confirmation.



Terms:

TTL - IP Time to Live - located in IP header of the DNS packet and is used effectively as a hop-count limit for the packet, to guard against routing loops


"shared" resource record set is where several Multicast DNS responders may have records with the same name, rrtype, and rrclass, and several responders may respond to a particular query

"unique" resource record set is one where all the records with that name, rrtype, and rrclass are conceptually under the control or ownership of a single responder, and it is expected that at most one responder should respond to a query for that name, rrtype, and rrclass. Before claiming ownership of a unique resource record set, a responder MUST probe to verify that no other responder already claims ownership. (For fault-tolerance and other reasons, sometimes it is permissible to have more than one responder answering for a particular "unique" resource record set, but such cooperating resoinders MUST give answers containing identical rdata for these records. If they do not give answers containing identical rdata, then the probing stop will reject the data as being inconsistent with what is already being advertised on the network for these names.) 


Multicast DNS Names

A host that belongs to an organization or individual who has control over some potion of the DNS namespace can be assigned a globally unique name within that portion of th DNS namespace, such as, "chechire.example.com". For those of us who have this luxury, this works very well. However, the mojority of home computer users do not have easy access to any portion of the global DNS namespace within which they have the authoriry to create names. This leaves the majority of home computers effectively anonymous for practical purposes









