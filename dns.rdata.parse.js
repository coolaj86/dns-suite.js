(function (exports) {
'use strict';

exports.DNS_RDATA_PARSE = function (ab, packet, record) {
  // ab is needed if the rdata makes use of compression pointers
  // packet is given for convenience
  var parser;


  if (!record.className) {
    throw new Error("Support for DNS Class 0x" + record.class.toString(16) + " (" + record.class + ")"
      + " is not implemented yet. Open an issue if you actually need support"
      + " (i.e. you're not working with a malformed packet)"
    );
  }

  if (!record.typeName) {
    throw new Error("Support for DNS Type 0x" + record.type.toString(16) + " (" + record.type + ")"
      + " is not implemented yet. Open an issue if you actually need support"
      + " (i.e. you're not working with a malformed packet)"
    );
  }

  try {
    parser = exports['DNS_PARSER_TYPE_' + record.typeName]
      || require('./parser/type.' + record.typeName.toLowerCase())['DNS_PARSER_TYPE_' + record.typeName];
  }
  catch (e) { /*console.error(e)*/ }

  if (!parser) {
    if (require) {
      require('./parser/type.' + record.typeName.toLowerCase());
    }
    throw new Error("Parser for DNS Type " + record.typeName + " could not be loaded."
      + " Did you include <script src=\"parser/type." + record.typeName.toLowerCase() + ".js\"></script> ?"
      + " (or perhaps we plan to implement it and haven't yet - in which case please open an issue)"
    );
  }

  // NOTE: record will be modified
  // Things that get added include:
  // address, data, priority exchange, weight,
  // NOTE: this slicing is a shim so that we don't have to pass rdend to unpackLabel
  // (because `undefined` and 0x00 are functionally equivalent)
  return parser(ab.slice(0, record.rdstart + record.rdlength), packet, record);
};

}('undefined' !== typeof window ? window : exports));
