(function (exports) {
'use strict';

var opcodes = exports.DNS_OPCODES = {
  QUERY:  0x00  //  0
};

// and in reverse
Object.keys(opcodes).forEach(function (key) {
  opcodes[opcodes[key]] = key;
});

}('undefined' !== typeof window ? window : exports));
