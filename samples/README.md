These samples were captured by running a generic udp listener and then running dig to create a query.

Most likely the 0th samples are the query from dig and the other immediately numbered samples are responses to that query...

However, devices broadcast mDNS packets pretty much at random, so there'll be some other goodies in there too.

```
dig @224.0.0.251 -p 5353 -t ptr _services._dns-sd._udp.local
dig @224.0.0.251 -p 5353 -t ptr _ssh._tcp.local
dig @224.0.0.251 -p 5353 -t ptr _sftp-ssh._tcp.local
dig @224.0.0.251 -p 5353 -t A bowie._sftp-ssh._tcp.local
```
