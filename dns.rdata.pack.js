(function (exports) {
'use strict';

var classes = exports.DNS_CLASSES || require('./dns.classes.js').DNS_CLASSES;
var types = exports.DNS_TYPES || require('./dns.types.js').DNS_TYPES;

exports.DNS_RDATA_PACK = function (ab, dv, total, record) {
  // ab is needed if the rdata makes use of compression pointers
  // packet is given for convenience
  var packer;
  var className = classes[record.class];
  var typeName = types[record.type];


  if (!className) {
    throw new Error("Support for DNS Class 0x" + record.class.toString(16) + " (" + record.class + ")"
      + " is not implemented yet. Open an issue if you actually need support"
      + " (i.e. you're not working with a malformed packet)"
    );
  }

  if (!typeName) {
    throw new Error("Support for DNS Type 0x" + record.type.toString(16) + " (" + record.type + ")"
      + " is not implemented yet. Open an issue if you actually need support"
      + " (i.e. you're not working with a malformed packet)"
    );
  }

  try {
    packer = exports['DNS_PACKER_TYPE_' + typeName]
      || require('./packer/type.' + typeName.toLowerCase())['DNS_PACKER_TYPE_' + typeName];
  }
  catch (e) { /*console.error(e)*/ }

  if (!packer) {
    throw new Error("Packer for DNS Type " + typeName + " could not be loaded."
      + " Did you include <script src=\"packer/type." + typeName.toLowerCase() + ".js\"></script> ?"
      + " (or perhaps we plan to implement it and haven't yet - in which case please open an issue)"
    );
  }

  return packer(ab, dv, total, record);
};

}('undefined' !== typeof window ? window : exports));
