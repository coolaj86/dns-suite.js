// NOTE: this should be EXACTLY the same as NS

(function (exports) {
'use strict';

// The host name that represents the supplied UP address
// May be a label, pointer or any combination

exports.DNS_PACKER_TYPE_PTR = function (ab, dv, total, record) {
  if (!record.data) {
    throw new Error("no data for PTR record");
  }

  // RDLENGTH
  // leading len and length of string and trailing null (all dots become lengths)
  dv.setUint16(total, record.data.length + 2, false);
  total += 2;

  // RDATA
  // a sequence of labels
  record.data.split('.').forEach(function (label) {

      dv.setUint8(total, label.length, false);
      total += 1;

      label.split('').forEach(function (ch) {
          dv.setUint8(total, ch.charCodeAt(0), false);
          total += 1;
      });
  });
  dv.setUint8(total, 0x00, false);
  total += 1;

  return total;
};

}('undefined' !== typeof window ? window : exports));
