(function (exports) {
'use strict';

// RFC 6844
// Explanation: https://tools.ietf.org/html/rfc6844#section-3
// Binary Format: https://tools.ietf.org/html/rfc6844#section-5
// Real-world Usage: https://support.dnsimple.com/articles/caa-record/

// A Certification Authority Authorization (CAA) record is used to specify which
// certificate authorities (CAs) are allowed to issue certificates for a domain.

// Value    Meaning/Use
//
// Flag     An unsigned integer between 0-255.
//          It is currently used to represent the critical flag, that has a
//          specific meaning per RFC 6844
// Tag      An ASCII string that represents the identifier of the property
//          represented by the record.
// Value    The value associated with the tag.

// The RFC currently defines 3 available tags:
//
// - issue:     explicity authorizes a single certificate authority to issue a
//              certificate (any type) for the hostname.
// - issuewild: explicity authorizes a single certificate authority to issue a
//              wildcard certificate (and only wildcard) for the hostname.
// - iodef:     specifies an URL to which a certificate authority may report
//              policy violations.

exports.DNS_PACKER_TYPE_CAA = function (ab, dv, total, record) {
  if ('number' !== typeof record.flag || isNaN(record.flag) || record.flag < 0 || record.flag > 255) {
    console.log(record);
    throw new Error("bad CAA flag:", record.flag);
  }
  if ('string' !== typeof record.tag || !record.tag || record.tag.length > 255) {
    throw new Error("bad CAA tag:", record.tag);
  }
  if ('string' !== typeof record.value || !record.value) {
    throw new Error("bad CAA value:", record.value);
  }

  // RDLEN = flag (1 byte) + taglen (1 byte) + tagstr (taglen bytes) + valuestr (valuelen bytes)
  dv.setUint16(total, 1 + 1 + record.tag.length + record.value.length, false);
  total += 2;

  // FLAG
  dv.setUint8(total, record.flag, false);
  total += 1;

  // TAG LENGTH
  dv.setUint8(total, record.tag.length, false);
  total += 1;

  // TAG
  record.tag.split('').forEach(function (ch) {
    dv.setUint8(total, ch.charCodeAt(0), false);
    total += 1;
  });

  // VALUE
  record.value.split('').forEach(function (ch) {
    dv.setUint8(total, ch.charCodeAt(0), false);
    total += 1;
  });

  return total;
};

}('undefined' !== typeof window ? window : exports));
