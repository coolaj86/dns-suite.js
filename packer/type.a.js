(function (exports) {
'use strict';

// An 'A' record is a 32-bit value representing the IP address

exports.DNS_PACKER_TYPE_A = function (ab, dv, total, record) {
  if (!record.address) {
    throw new Error("no address on A record");
  }

  // RDLENGTH
  var IP_LEN = 4;
  dv.setUint16(total, IP_LEN, false);
  total += 2;

  // RDATA
  // i.e. 127.0.0.1 => 0x7F, 0x00, 0x00, 0x01
  record.address.split('.').forEach(function (octet) {
    dv.setUint8(total, parseInt(octet, 10), false);
    total += 1;
  });

  return total;
};

}('undefined' !== typeof window ? window : exports));
