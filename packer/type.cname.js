(function (exports) {
'use strict';

// A 'CNAME' record is a 32-bit value representing the IP address

exports.DNS_PACKER_TYPE_CNAME = function (ab, dv, total, record) {
  if (!record.data) {
    throw new Error("no data for CNAME record");
  }

  var cnameLen = 0;
  var rdLenIndex = total;
  total += 2;

  // RDATA
  // a sequence of labels
  record.data.split('.').forEach(function (label) {
    cnameLen += 1 + label.length;

    dv.setUint8(total, label.length, false);
    total += 1;

    label.split('').forEach(function (ch) {
      dv.setUint8(total, ch.charCodeAt(0), false);
      total += 1;
    });
  });
  dv.setUint8(total, 0x00, false);
  total += 1;

  // RDLENGTH
  dv.setUint16(rdLenIndex, record.data.length + 2, false);

  return total;
};

}('undefined' !== typeof window ? window : exports));
