(function (exports) {
'use strict';

// SRV RDATA contains:
// Priority: The relative priority of this service. 16-bit (range 0-65535)
// Weight:   Used when more than one serivice has the same priority. 16-bit
//           (range 0-65535)
// Port:     Port number assigned to the symbolic service. 16-bit (range 0-65535)
// Target:   The name of the host that will provide service.

exports.DNS_PACKER_TYPE_SRV = function (ab, dv, total, record) {

  // maybe these should be changed to 'hasOwnProperty' for all of these
  // TODO: Check that number is in range 1-64k
  if (!record.priority){
    throw new Error("no priority for SRV record");
  }
  if (!record.hasOwnProperty('weight')){
    throw new Error("no weight for SRV record");
  }
  if (!record.port){
    throw new Error("no port for SRV record");
  }
  if (!record.target) {
    throw new Error("no target for SRV record");
  }

  // console.log("record length, priority, weight, port, then target");
  // console.log("record priority is: " + record.priority);
  // console.log("record weight is: " + record.weight);
  // console.log("record port is: " + record.port);
  // console.log("record target is: " + record.target);
  // console.log("total length currently is: " + total);


  var srvLen = 6; // 16-bit priority, weight and port = 6 Bytes
  var rdLenIndex = total;

  total+=2; // space for RDLENGTH

  dv.setUint16(total, parseInt(record.priority, 10), false);
  total+=2;

  dv.setUint16(total,parseInt(record.weight, 10), false);
  total+=2;

  dv.setUint16(total, parseInt(record.port, 10), false);
  total+=2;

  record.target.split('.').forEach(function (label){
    srvLen += 1 + label.length;

    dv.setUint8(total, label.length, false);
    total += 1;

    label.split('').forEach(function (ch) {
      dv.setUint8(total, ch.charCodeAt(0), false);
      total += 1;
    });
  });

  dv.setUint8(total, 0x00, false);
  total += 1;
  srvLen += 1;

  // RDLENGTH

  dv.setUint16(rdLenIndex, srvLen, false);

  return total;
};

}('undefined' !== typeof window ? window : exports));
