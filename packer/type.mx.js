(function (exports) {
'use strict';

// An 'MX' record is a 32-bit value representing the IP address

exports.DNS_PACKER_TYPE_MX = function (ab, dv, total, record) {
  if (!record.exchange) {
    throw new Error("no exchange for MX record");
  }
  if (!record.priority) {
    // TODO: Check that number is in range 1-64k
    throw new Error("no priority for MX record");
  }

  var mxLen = 2; // 16-bit priority plus label sequence
  var rdLenIndex = total;
  total += 2; // The space for RDLENGTH is reserved now and set below
  dv.setUint16(total, parseInt(record.priority, 10), false);
  total += 2;

  // RDATA
  // 16-bit priority and a sequence of labels as the exchange
  record.exchange.split('.').forEach(function (label) {
    mxLen += 1 + label.length;

    dv.setUint8(total, label.length, false);
    total += 1;

    label.split('').forEach(function (ch) {
      dv.setUint8(total, ch.charCodeAt(0), false);
      total += 1;
    });
  });
  dv.setUint8(total, 0x00, false);
  total += 1;
  mxLen += 1;

  // RDLENGTH
  dv.setUint16(rdLenIndex, mxLen, false);

  return total;
};

}('undefined' !== typeof window ? window : exports));
