(function (exports) {
'use strict';

// Record type is just any text.
// RFC 1035 - 3.3. Standard RRs - <character-string>
// RFC 1035 - 3.3.14. TXT RDATA format

exports.DNS_PACKER_TYPE_TXT = function (ab, dv, total, record) {
    if ('string' === typeof record.data) {
      console.warn("[WARN] 'data' should be an array of strings, not a single string");
      record.data = [ record.data ];
    }
    if (!Array.isArray(record.data) || !record.data.length){
        throw new Error("bad data or no data for TXT record");
    }

    var txtLen = 0;
    var rdLenIndex = total;
    total += 2; // saving space for rdata length

    // RDATA
    record.data.forEach(function(str){

      if (str.length > 255) {
        throw new Error("bad TXT string length " + str.length + " (max 255)");
      }

      // size of this string
      dv.setUint8(total, str.length, false);
      total += 1;
      txtLen += 1;

      str.split('').forEach(function(ch) {

        dv.setUint8(total, ch.charCodeAt(0), false);
        total += 1;
        txtLen += 1;

      });
    });

    dv.setUint16(rdLenIndex, txtLen, false);

    return total;
};

}('undefined' !== typeof window ? window : exports));
