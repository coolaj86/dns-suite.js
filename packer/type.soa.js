(function (exports) {
'use strict';

// http://www.zytrax.com/books/dns/ch8/soa.html
// https://github.com/tjfontaine/node-dns#resourcerecord

// Value              Meaning/Use
// Primary NS         Variable length. The name of the Primary Master for the domain. May be a label, pointer, or any combination
// Admin MB           Variable length. The administrator's mailbox. May be a label, pointer, or any combination
// Serial Number      Unsigned 32-bit integer
// Refresh Interval   Unsigned 32-bit integer
// Retry Interval     Unsigned 32-bit integer
// Expiration Limit   Unsigned 32-bit integer
// Minimum TTL        Unsigned 32-bit integer

exports.DNS_PACKER_TYPE_SOA = function (ab, dv, total, record) {
  function notNumber(n) {
    return 'number' !== typeof n || isNaN(n);
  }

  if (!record.primary && !record.name_server) {
    throw new Error("no name server for SOA record");
  }
  if (!record.admin && !record.email_addr) {
    throw new Error("ne email address for SOA record");
  }
  if (notNumber(record.serial) && notNumber(record.sn)) {
    throw new Error("no serial number for SOA record");
  }
  if (notNumber(record.refresh) && notNumber(record.ref)) {
    throw new Error("no refresh for SOA record");
  }
  if (notNumber(record.retry) && notNumber(record.ret)) {
    throw new Error("no update retry for SOA record");
  }
  if (notNumber(record.expiration) && notNumber(record.ex)) {
    throw new Error("no expiry for SOA record");
  }
  if (notNumber(record.minimum) && notNumber(record.nx)) {
    throw new Error("no nxdomain for SOA record");
  }

  var soaLen = 20; // take into account sn, ref, ret, ex, and nx
                  //  (32-bits each. 4Bytes * 5 = 20)
  var rdLenIndex = total;
  total += 2; // Save space for RDLENGTH

  //console.log('record.name_server', 1 + record.name_server.length, record.name_server);
  // pack name_server which is a sequence of labels
  (record.primary || record.name_server).split('.').forEach(function (label) {
    soaLen += (1 + label.length);

    dv.setUint8(total, label.length, false);
    total += 1;

    label.split('').forEach(function(ch) {
      dv.setUint8(total, ch.charCodeAt(0), false);
      total += 1;
    });
  });
  // must be terminated by null when not using write null
  dv.setUint8(total, 0, false);
  total += 1;
  soaLen += 1;

  //console.log('record.email_addr', 1 + record.email_addr.length, record.email_addr);
  // pack email address which is a sequence of labels
  (record.admin || record.email_addr).split('.').forEach(function (label) {
    soaLen += 1 + label.length;

    dv.setUint8(total, label.length, false);
    total += 1;

    label.split('').forEach(function (ch){
      dv.setUint8(total, ch.charCodeAt(0), false);
      total += 1;
    });
  });
  // must be terminated by null when not using write null
  dv.setUint8(total, 0, false);
  total += 1;
  soaLen += 1;

  // pack all 32-bit values
  dv.setUint32(total, parseInt(record.serial || record.sn, 10), false);
  total+=4;
  dv.setUint32(total, parseInt(record.refresh || record.ref, 10), false);
  total+=4;
  dv.setUint32(total, parseInt(record.retry || record.ret, 10), false);
  total+=4;
  dv.setUint32(total, parseInt(record.expiration || record.ex, 10), false);
  total+=4;
  dv.setUint32(total, parseInt(record.minimum || record.nx, 10), false);
  total+=4;

  // RDLENGTH
  //console.log('rdAt', rdLenIndex);
  //console.log('soaLen (lables + 2 + 20)', soaLen);
  dv.setUint16(rdLenIndex, soaLen, false);

  return total;
};

}('undefined' !== typeof window ? window : exports));
