(function (exports) {
'use strict';

// 'AAAA'
// Value: IP Address
// Meaning:Use: 16 octets (8 sexdectets, 128 bits) represting the IP address

exports.DNS_PACKER_TYPE_AAAA = function (ab, dv, total, record) {
  if (!record.address) {
    throw new Error("no address on A record");
  }

  // i.e. ::1 => 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0001
  // i.e. FFFF:DDDD::1 => 0xFFFF, 0xDDDD, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0001
  //record.rdlength = 16;
  var address = '0' + record.address; // prevent case of two leading blanks, such as '::1'
  var parts = address.split(':');
  parts.forEach(function (sedectet, i) {
    if (!sedectet) {
      parts[i] = '0';
      while (parts.length < 8) {
        parts.splice(i + 1, 0, '0');
      }
    }
  });

  // RDLENGTH
  var IP_LEN = 16;
  dv.setUint16(total, IP_LEN, false);
  total += 2;

  // RDATA
  parts.forEach(function (sedectet) {
    dv.setUint16(total, parseInt(sedectet, 16), false);
    total += 2;
  });

  return total;
};

}('undefined' !== typeof window ? window : exports));
