'use strict';

var fs = require('fs');
var path = require('path');
var dnsjs = require('../').DNSPacket;
var expected; // shim

var dirname = path.join(__dirname, 'fixtures');
var i = 0;

console.log("\nConverting .js fixtures to .json\n");
fs.readdirSync(dirname).forEach(function (name) {
  if (!/\.js$/.test(name)) {
    return;
  }

  var filename = path.join(dirname, name);

  i += 1;
  console.log('•', i, 'test/fixtures/' + name);

  var js = "module.exports = " + fs.readFileSync(filename, 'utf8');
  fs.writeFileSync(filename + '.real.js', js);
  var json = require(filename + '.real.js');
  fs.writeFileSync(filename + 'on', JSON.stringify(json, null, 2), 'utf8');
  try {
    fs.unlinkSync(filename + '.real.js');
  } catch(e) {
    console.error('Error:', filename);
    console.error(e.message + '\n');
  }
});
console.log("\nDone.\n\n\n");
