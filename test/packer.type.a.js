'use strict';

var packer = require('../packer/type.a.js').DNS_PACKER_TYPE_A;
var ab = new ArrayBuffer(6); // 16-bit RDLENGTH + 32-bit address
var dv = new DataView(ab);
var total;

// just to see that bytes are changed as a marker
dv.setUint32(0x0, 0xDDDDDDDD, false);

[ '0.0.0.0'
, '127.0.0.1'
, '192.168.1.100'
, '216.21.170.44'
, '255.255.255.255'
].forEach(function (ipv4) {
  total = 0;
  total = packer(ab, dv, total, { address: ipv4 });
  if (0x06 !== total) {
    console.error('unexpected total ' + total);
    process.exit(1);
  }
  // 0.4 is just a hacky way to account for the RDLENGTH
  if ('0.4.' + ipv4 !== new Uint8Array(ab).join('.')) {
    console.error("expected: ", ipv4);
    console.error("actual: ", new Uint8Array(ab).join('.'));
    process.exit(1);
  }
});

console.log('PASS');
