'use strict';

var packer = require('../packer/type.cname.js').DNS_PACKER_TYPE_CNAME;
var ab = new ArrayBuffer(254); // max len per label is 63, max label sequence is 254
var dv = new DataView(ab);
var total = 0;

[ [ 'www.example.com'
  , [ 0x00, 0x10, 0x03, 119, 119, 119, 0x07, 101, 120, 97, 109, 112, 108, 101, 0x03, 99, 111, 109 ].join(' ') ]
].forEach(function (cname) {
  total = packer(ab, dv, total, { data: cname[0] });
  ab = ab.slice(0, total);
  // TODO: Check that total increments appropriately
  if (cname[1] !== new Uint8Array(ab).join(' ')) {
    console.error("expected: ", cname[1]);
    console.error("actual: ", new Uint8Array(ab).join(' '));
    process.exit(1);
  }
});

console.log('PASS');
