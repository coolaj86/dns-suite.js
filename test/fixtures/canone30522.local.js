module.exports = {
    "header": {
        "id": 0,
        "qr": 1,
        "opcode": 0,
        "aa": 0,
        "tc": 0,
        "rd": 0,
        "ra": 0,
        "res1": 0,
        "res2": 0,
        "res3": 0,
        "rcode": 0
    },
    "question": [],
    "answer": [
        {
            "name": "_pdl-datastream._tcp.local",
            "type": 12,
            "class": 1,
            "ttl": 255,
            "data": "Canon MF620C Series._pdl-datastream._tcp.local"
        }
    ],
    "additional": [
        {
            "name": "Canone30522.local",
            "type": 1,
            "class": 32769,
            "ttl": 255,
        },
        {
            "name": "Canon MF620C Series._pdl-datastream._tcp.local",
            "type": 33,
            "class": 32769,
            "ttl": 255,
        },
        {
            "name": "Canon MF620C Series._pdl-datastream._tcp.local",
            "type": 16,
            "class": 32769,
            "ttl": 255,
        },
        {
            "name": "Canone30522.local",
            "type": 47,
            "class": 32769,
            "ttl": 255,
        },
        {
            "name": "Canon MF620C Series._pdl-datastream._tcp.local",
            "type": 47,
            "class": 32769,
            "ttl": 255,
        }
    ],
    "authority": [],
    "edns_options": []
};
