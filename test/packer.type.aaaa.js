'use strict';

var packer = require('../packer/type.aaaa.js').DNS_PACKER_TYPE_AAAA;
var ab = new ArrayBuffer(22);
var dv = new DataView(ab);
var total = 0;

// just to see that bytes are changed as a marker
dv.setUint16(0x0, 0xDDDD, false);
dv.setUint16(0x2, 0xDDDD, false);
dv.setUint16(0x4, 0xDDDD, false);
dv.setUint16(0x6, 0xDDDD, false);
dv.setUint16(0x8, 0xDDDD, false);
dv.setUint16(0xA, 0xDDDD, false);
dv.setUint16(0xC, 0xDDDD, false);
dv.setUint16(0xE, 0xDDDD, false);
dv.setUint16(0x10, 0xDDDD, false);
dv.setUint16(0x12, 0xDDDD, false);
dv.setUint16(0x14, 0xDDDD, false);

[ [ '::1'
  , [ 0xDD, 0xDD, 0x00, 0x10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0xDD, 0xDD ].join(' ') ]
, [ 'fe80::1'
  , [ 0xDD, 0xDD, 0x00, 0x10, 0xFE, 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0xDD, 0xDD ].join(' ') ]
, [ 'fd41:50ca:2169:f4e:5020:f756:ca4e:c3b5'
  , [ 0xDD, 0xDD, 0x00, 0x10, 0xFD, 0x41, 0x50, 0xCA, 0x21, 0x69, 0x0F, 0x4E
    , 0x50, 0x20, 0xF7, 0x56, 0xCA, 0x4E, 0xC3, 0xB5, 0xDD, 0xDD ].join(' ') ]
].forEach(function (ipv6) {
  total = 2; // leave leading 0xDD
  total = packer(ab, dv, total, { address: ipv6[0] });
  if (0x14 !== total) {
    console.error('unexpected total ' + total);
    process.exit(1);
  }
  if (ipv6[1] !== new Uint8Array(ab).join(' ')) {
    console.error("expected: ", ipv6[1]);
    console.error("actual: ", new Uint8Array(ab).join(' '));
    process.exit(1);
  }
});

console.log('PASS');
