#!/bin/bash

# A sanity check to prevent human error
echo "if you see any output, this test has failed"

grep -R setUint *.js test/ | grep -v false
grep -R getUint *.js test/ | grep -v false
