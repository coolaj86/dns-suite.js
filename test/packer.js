;(function () {
  'use strict';

  var fs = require('fs');
  //var path = require('path');
  var dnsjs = require('../').DNSPacket;

  //var dirname = path.join(__dirname, 'fixtures');
  //var expected; // shim
  var onefile = process.argv[2];

  if (!onefile) {
    console.error('');
    console.error('Usage:');
    console.error('node test/packer.js <test/fixtures/packet.type.json>');
    console.error('Example:');
    console.error('node test/packer.js test/fixtures/www.linode.com.a.json');
    console.error('');
    process.exit(1);
  }

  var json = JSON.parse(fs.readFileSync(onefile, 'utf8'));

  var ab = dnsjs.write(json).buffer;
  //console.log(ab);
  //console.log(ui8);

  function hexdump(ab) {
    var ui8 = new Uint8Array(ab);
    var bytecount = 0;
    var head = '        0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F';
    var trail;
    var str = [].slice.call(ui8).map(function (i) {
      var h = i.toString(16);
      if (h.length < 2) {
        h = '0' + h;
      }
      return h;
    }).join('').match(/.{1,2}/g).join(' ').match(/.{1,48}/g).map(function (str) {
      var lead = bytecount.toString(16);
      bytecount += 16;

      while (lead.length < 7) {
        lead = '0' + lead;
      }

      return lead + ' ' + str;
    }).join('\n');
    trail = ab.byteLength.toString(16);
    while (trail.length < 7) {
      trail = '0' + trail;
    }
    return head + '\n' + str + '\n' + trail;
  }

  console.log('');
  console.log('DEBUG with hexdump: ');
  console.log('hexdump ' + onefile.replace(/\.[^\.]*$/, '.bin'));
  console.log('');
  console.log(hexdump(ab));
  console.log('');

  console.error('');
  console.error('!!!');
  console.error('Test implementation not complete.');
  console.error('!!!');
  console.error('');
  process.exit(1);

}());

