'use strict';

var packer = require('../packer/type.mx.js').DNS_PACKER_TYPE_MX;
var ab = new ArrayBuffer(256); // priority + max len per label is 63, max label sequence is 254
var dv = new DataView(ab);
var total = 0;

[ [ '10:www.example.com'
  , [ 0x00, 0x12, 0x00, 0x0A, 0x03, 119, 119, 119, 0x07, 101, 120, 97, 109, 112, 108, 101, 0x03, 99, 111, 109 ].join(' ') ]
].forEach(function (mx) {
  total = packer(ab, dv, total, { priority: mx[0].split(':')[0], exchange: mx[0].split(':')[1] });
  ab = ab.slice(0, total);
  // TODO: Check that total increments appropriately
  if (mx[1] !== new Uint8Array(ab).join(' ')) {
    console.error("expected: ", mx[1]);
    console.error("actual: ", new Uint8Array(ab).join(' '));
    process.exit(1);
  }
});

console.log('PASS');
