;(function () {
  //'use strict';

  require('./convert-fixtures-to-json.js');

  var assert = require('assert');
  var fs = require('fs');
  var path = require('path');
  var dnsjs = require('../').DNSPacket;
  var expected; // shim
  var onefile = process.argv[2];

  var dirname = path.join(__dirname, 'fixtures');

  function deepLike(expected, actual, prevKey) {
    prevKey = prevKey || '';

    return Object.keys(expected).every(function (key) {
      var val = expected[key];

      if (val && 'object' === typeof val && !Array.isArray(val)) {
        if ('undefined' === typeof actual[key]) {
          throw new Error("[" + prevKey + key + "] is missing from actual, but seen in expected");
        }
        return deepLike(expected[key], actual[key], prevKey + '.');
      }

      if (Array.isArray(val)) {
        // assumes that arrays do not contain non-primitive values (no nested objects or arrays)
        if (!val[0] || 'object' !== typeof val[0]) {
          return assert.deepEqual(val, actual[key]);
        }
        return deepLike(val, actual[key]);
      }

      if (val !== actual[key]) {
        // value should always be a primitive by now (we do not expect functions)
        throw new Error("[" + prevKey + key + "] actual is not equal to expected: " + expected[key] + ' : ' + actual[key]);
      }

      return true;
    });
  }

  fs.readdirSync(dirname).forEach(function (name) {
    if (onefile && !onefile.split(',').some(function (f) { return name.match(onefile); })) {
      return;
    }
    if (!/\.bin$/.test(name)) {
      return;
    }

    var filename = path.join(dirname, name);

    var expected = require(path.join(dirname, name.replace(/\.bin$/, '.json')));
    var nb = fs.readFileSync(filename, null);
    console.log('Testing ' + filename);

    var ab = nb.buffer.slice(nb.byteOffset, nb.byteOffset + nb.byteLength);
    var result;

    try {
      result = dnsjs.parse(ab);
    } catch(e) {
      console.error('[Error] parse error');
      console.log(ab);
      console.error(e.packet);
      console.error(e.record);
      console.error(e.stack);
      console.error('');
      //console.error(e.stack);
      return;
    }

    // TODO deepHas
    // compare two objects and make sure that the second has all of what the first has (but perhaps more)

    try {
      deepLike(expected, result);
    } catch(e) {
      console.log('');
      console.log('[RESULT]');
      console.log(JSON.stringify(result, null, 2));
      console.error('[FAIL]', name);
      console.error(e.stack);
      console.log('');
    }
  });
}());
