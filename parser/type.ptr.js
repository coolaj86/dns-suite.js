(function (exports) {
'use strict';

// Pointer records are the opposite of A and AAAA and are
// used in Reverse Map zone files to map an IP address (IPv4 or IPv6)
// to a host name.

// FORMAT:
// name   ttl   class   rr     name
// foo.   15    IN      PTR    www.example.com.

var unpackLabels = exports.DNS_UNPACK_LABELS || require('../dns.unpack-labels.js').DNS_UNPACK_LABELS;
exports.DNS_PARSER_TYPE_PTR = function (ab, pack, record) {
  var labelInfo = unpackLabels(new Uint8Array(ab), record.rdstart, { byteLength: 0, cpcount: 0, labels: [], name: '' });
  if (record.trunc) {
    throw new Error("RDATA type PTR must be `null`-terminated, not truncated by rdata length.");
  }
  record.data = labelInfo.name;
};

}('undefined' !== typeof window ? window : exports));
