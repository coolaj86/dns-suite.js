(function (exports) {
'use strict';

// Value: Preference
// Meaning/Use: Unsigned 16-bit integer
//-------------------------------------
// Value: Mail Exchanger
// Meaning/Use: The name host name that provides the service.
// May be a label, pointer or any combination

// ab is arrayBuffer, packet is Object, Record is Object
var unpackLabels = exports.DNS_UNPACK_LABELS || require('../dns.unpack-labels.js').DNS_UNPACK_LABELS;
exports.DNS_PARSER_TYPE_MX = function (ab, packet, record) {

  var rdataAb = ab.slice(record.rdstart, record.rdstart + record.rdlength);
  var dv = new DataView(rdataAb);

  record.priority =  dv.getUint16(0, false);
  record.exchange = unpackLabels(new Uint8Array(ab), record.rdstart+2, { byteLength: 0, cpcount: 0, labels: [], name: '' }).name;

  return record;

};

}('undefined' !== typeof window ? window : exports));

