(function (exports) {
'use strict';

// RFC 6844 https://tools.ietf.org/html/rfc6844#section-3
// https://support.dnsimple.com/articles/caa-record/

// A Certification Authority Authorization (CAA) record is used to specify which
// certificate authorities (CAs) are allowed to issue certificates for a domain.

// Value    Meaning/Use
//
// Flag     An unsigned integer between 0-255.
//          It is currently used to represent the critical flag, that has a
//          specific meaning per RFC 6844
// Tag      An ASCII string that represents the identifier of the property
//          represented by the record.
// Value    The value associated with the tag.

// The RFC currently defines 3 available tags:
//
// - issue:     explicity authorizes a single certificate authority to issue a
//              certificate (any type) for the hostname.
// - issuewild: explicity authorizes a single certificate authority to issue a
//              wildcard certificate (and only wildcard) for the hostname.
// - iodef:     specifies an URL to which a certificate authority may report
//              policy violations.

exports.DNS_PARSER_TYPE_CAA = function (ab, packet, record) {

  var data = new Uint8Array(ab);
  var i = record.rdstart;
  var flag = data[i];
  var mid = data[i + 1];
  i += 2;
  mid += i;
  var end = record.rdstart + record.rdlength;
  var tag = '';
  var value = '';

  while (i < mid) {
    tag += String.fromCharCode(data[i]);
    i += 1;
  }

  while (i < end) {
    value += String.fromCharCode(data[i]);
    i += 1;
  }

  record.flag = flag;
  record.tag = tag;
  record.value = value;

  return record;
};

}('undefined' !== typeof window ? window : exports));
