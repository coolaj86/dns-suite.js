(function (exports) {
'use strict';

// FORMAT:
// name   ttl   class   rr     name
// foo.   15    IN      NS     www.example.com.

// Comes in variable lengths. It is the name of the primary Master for the Domain.
// For example 'ns1.example.com'
// It may be a label, pointer or any combination
var unpackLabels = exports.DNS_UNPACK_LABELS || require('../dns.unpack-labels.js').DNS_UNPACK_LABELS;


exports.DNS_PARSER_TYPE_NS = function (ab , packet, record) {
  var labelInfo = unpackLabels(new Uint8Array(ab), record.rdstart, { byteLength: 0, cpcount: 0, labels: [], name: '' });
  if (record.trunc) {
    throw new Error("RDATA type NS must be `null`-terminated, not truncated by rdata length.");
  }
  record.data = labelInfo.name;
  return record;
};

}('undefined' !== typeof window ? window : exports));
