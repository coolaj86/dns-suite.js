(function (exports) {
'use strict';

// A CNAME reocord maps a single alias or nickname to the real or
// Canonical name which may lie outside the current zone.
// Canonical simply means the expected or real name.

var unpackLabels = exports.DNS_UNPACK_LABELS || require('../dns.unpack-labels.js').DNS_UNPACK_LABELS;
exports.DNS_PARSER_TYPE_CNAME = function (ab, packet, record) {
  record.data = unpackLabels(new Uint8Array(ab), record.rdstart, { byteLength: 0, cpcount: 0, labels: [], name: '' }).name;
  return record;
};

}('undefined' !== typeof window ? window : exports));
