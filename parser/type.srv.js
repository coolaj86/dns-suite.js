(function (exports) {
'use strict';

// SRV identifies the host(s) that will support a particular service. It
// is a general purpose RR to discover any service.
var unpackLabels = exports.DNS_UNPACK_LABELS || require('../dns.unpack-labels.js').DNS_UNPACK_LABELS;


exports.DNS_PARSER_TYPE_SRV = function (ab, packet, record) {

    var rdataAb = ab.slice(record.rdstart,record.rdstart + record.rdlength)
    var dv = new DataView(rdataAb);

    record.priority = dv.getUint16(0, false);
    record.weight = dv.getUint16(2, false);
    record.port = dv.getUint16(4, false);
    record.target = unpackLabels(new Uint8Array(ab), record.rdstart+6, { byteLength: 0, cpcount: 0, labels: [], name: '' }).name;

    return record;

};
}('undefined' !== typeof window ? window : exports));
