(function (exports) {
'use strict';

// An 'A' record is a 32-bit value representing the IP address

exports.DNS_PARSER_TYPE_A = function (ab, packet, record) {
  var ui8 = new Uint8Array(ab.slice(record.rdstart, record.rdstart + record.rdlength));
  // i.e. 127.0.0.1
  record.address = ui8[0] + '.' + ui8[1] + '.' + ui8[2] + '.' + ui8[3];

  return record;
};

}('undefined' !== typeof window ? window : exports));
