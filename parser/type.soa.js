(function (exports) {
'use strict';

// Value              Meaning/Use
// Primary NS         Variable length. The name of the Primary Master for the domain. May be a label, pointer, or any combination
// Admin MB           Variable length. The administrator's mailbox. May be a label, pointer, or any combination
// Serial Number      Unsigned 32-bit integer
// Refresh Interval   Unsigned 32-bit integer
// Retry Interval     Unsigned 32-bit integer
// Expiration Limit   Unsigned 32-bit integer
// Minimum TTL        Unsigned 32-bit integer

var unpackLabels = exports.DNS_UNPACK_LABELS || require('../dns.unpack-labels.js').DNS_UNPACK_LABELS;

exports.DNS_PARSER_TYPE_SOA = function (ab, packet, record) {
  //
  // Look!
  // http://www.peerwisdom.org/2013/05/15/dns-understanding-the-soa-record/

  var rdataAb = ab.slice(record.rdstart, record.rdstart + record.rdlength);
  var dv = new DataView(rdataAb);

  // we need this information for this parser
  var labelInfo = unpackLabels(new Uint8Array(ab), record.rdstart, { byteLength: 0, cpcount: 0, labels: [], name: '' });
  //var cpcount = labelInfo.cpcount;
  var offset = labelInfo.byteLength;
  //var labels = labelInfo.labels;

  // Primary NS
  record.primary = labelInfo.name;
  record.name_server = record.primary;

  // TODO delete this commented out code after some testing
  // (pretty sure it was unnecessary and it seemed to work on code with compression pointers just fine)
  /*
  // if there exists compression pointers in the rdata
  if (cpcount > 0) {
    // do something awesome with compression pointers to get the email address
    // I need the length of all the data before the email address starts.
    // if there are compression pointers then there will be a byte to indicate the length of each label, the label,
    // then there will be a compression pointer to grab the longest label.

    var start = 2; // start or email_addr. take into account compression pointer and address length
    for (var i = 0; i < labels.length; i += 1) {

      // increase start by the label length. the +1 is to take into account the next label size byte
      start = start + labels[i].length + 1;
      // check for cpcount. 2 counts behind
      if (parseInt(dv.getUint8(start - 2), 10) === 192) {
        record.email_addr = unpackLabels(new Uint8Array(ab), record.rdstart + start ,{ byteLength: 0, cpcount: 0, labels: [], name: '' }).name;
      }
    }
  } // if there are no compression pointers, we can get the email address directly from the offset
  else {

    record.email_addr = unpackLabels(new Uint8Array(ab), record.rdstart + offset, { byteLength: 0, cpcount: 0, labels: [], name: '' }).name;
  }
  */
  record.admin = unpackLabels(new Uint8Array(ab), record.rdstart + offset, { byteLength: 0, cpcount: 0, labels: [], name: '' }).name;
  record.email_addr = record.admin;

  // Serial Number
  record.serial = record.sn = dv.getUint32(dv.byteLength - 20, false);
  // Refresh Interval
  record.refresh = record.ref = dv.getUint32(dv.byteLength - 16, false);
  // Retry Interval
  record.retry = record.ret = dv.getUint32(dv.byteLength - 12, false);
  // Expiration Limit
  record.expiration = record.ex = dv.getUint32(dv.byteLength - 8, false);
  // Minimum TTL
  record.minimum = record.nx = dv.getUint32(dv.byteLength - 4, false);

  return record;
};

}('undefined' !== typeof window ? window : exports));
