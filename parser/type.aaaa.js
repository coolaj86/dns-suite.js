(function (exports) {
'use strict';

// Value: IP Address
// Meaning:Use: 16 octets represting the IP address

exports.DNS_PARSER_TYPE_AAAA = function (ab, packet, record) {

    var rdataAb = ab.slice(record.rdstart, record.rdstart + record.rdlength);
    // We can't use Uint16Array because it doesn't specify Endianness
    // Intel x86, x64, and (usually) ARM are Little Endian, but Network Order is Big Endian
    // DataView *does* let us specify endianness, so we can use that
    // http://stackoverflow.com/questions/13514614/why-is-network-byte-order-defined-to-be-big-endian
    var dv = new DataView(rdataAb);
    var i = 0;
    var s = '';

    // Note: byteLength is always 16 for AAAA records
    for (i = 0; i < dv.byteLength; i += 2) {

      if (i > 0 && i < dv.byteLength) {
        // add ':' between all... sedectets? sexdectets? ... pairs of two octets :)
        s += ':';
      }

      // Big Endian Uint16 is specified by using `false`, Little Endian is `true`
      s += dv.getUint16(i, false).toString(16);
    }

    // Represent the string address as recommended on the wikipedia page
    // https://en.wikipedia.org/wiki/IPv6_address#Recommended_representation_as_text.
    // (shorten the longest section of 0's as long as it's more than one section, replacing
    // the left-most instance in the event of ties.)
    var re = /:(0:)+/g;
    var match;
    var longest = '_BAD';
    while (!!(match = re.exec(s))) {
      if (match[0].length > longest.length) {
        longest = match[0];
      }
    }
    s = s.replace(longest, '::');

    record.address = s;
    return record;
};

}('undefined' !== typeof window ? window : exports));
