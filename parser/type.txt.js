(function (exports) {
'use strict';

// Used to provide the ability to associate some arbitrary and unformatted text
// with a host or other name, such as a human readable information about a server
// network, data center, and other accounting information.

var unpackLabels = exports.DNS_UNPACK_LABELS || require('../dns.unpack-labels.js').DNS_UNPACK_LABELS;

exports.DNS_PARSER_TYPE_TXT = function (ab, packet, record) {

  var labels = unpackLabels(new Uint8Array(ab), record.rdstart, { byteLength: 0, cpcount: 0, labels: [], name: '' });
  record.data = [ labels.name ];

  return record;
};

}('undefined' !== typeof window ? window : exports));
