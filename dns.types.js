(function (exports) {
'use strict';

var types = exports.DNS_TYPES = {
  A:           0x1 // 1
, NS:          0x2 // 2
, CNAME:       0x5 // 5
, SOA:         0x6 // 6
, NULL:        0xa // 10
, PTR:         0xc // 12
, HINFO:       0xd // 13
, MX:          0xf // 15
, TXT:        0x10 // 16
, RP:         0x11 // 17
, AFSDB:      0x12 // 18
, SIG:        0x18 // 24
, KEY:        0x19 // 25
, AAAA:       0x1c // 28
, LOC:        0x1d // 29
, SRV:        0x21 // 33
, NAPTR:      0x23 // 35
, KX:         0x24 // 36
, CERT:       0x25 // 37
, DNAME:      0x27 // 39
, OPT:        0x29 // 41
, APL:        0x2a // 42
, DS:         0x2b // 43
, SSHFP:      0x2c // 44
, IPSECKEY:   0x2d // 45
, RRSIG:      0x2e // 46
, NSEC:       0x2f // 47
, DNSKEY:     0x30 // 48
, DHCID:      0x31 // 49
, NSEC3:      0x32 // 50
, NSEC3PARAM: 0x33 // 51
, TLSA:       0x34 // 52
, HIP:        0x37 // 55
, CDS:        0x3b // 59
, CDNSKEY:    0x3c // 60
, SPF:        0x63 // 99
, TKEY:       0xf9 // 249
, TSIG:       0xfa // 250
, IXFR:       0xfb // 251
, AXFR:       0xfc // 252
, ANY:        0xff // 255
, CAA:       0x101 // 257
, TA:       0x8000 // 32768
, DLV:      0x8001 // 32769
};

// and in reverse
Object.keys(types).forEach(function (key) {
  types[types[key]] = key;
});

}('undefined' !== typeof window ? window : exports));
