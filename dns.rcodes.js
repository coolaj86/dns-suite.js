(function (exports) {
'use strict';

var rcodes = exports.DNS_RCODES = {
  SUCCESS:  0x00  //   0
, NXDOMAIN: 0x03  //   3
, REFUSED:  0x05  //   5
};

// and in reverse
Object.keys(rcodes).forEach(function (key) {
  rcodes[rcodes[key]] = key;
});

}('undefined' !== typeof window ? window : exports));
